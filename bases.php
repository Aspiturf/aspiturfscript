<?php
/**
* This file is part of Aspiturf.
*
* Aspiturf is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Aspiturf is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Aspiturf.  If not, see <http://www.gnu.org/licenses/>.
**/


echo "base chargée";

	class MyTable
	{
		var $connexion;
		var $requete;
		var $result_set;
		var $table;
		var $base;
		var $colonnes_rc;
		var $colonnes;
		var $id;
		var $where;

		function MyTable($base,$connect,$user,$pass,$req)
		{

			// préparation de la requête
			$this->requete="$req";
		//	$this->table=$table;
			$this->base=$base;

			// connexion à la base
			$this->connexion=mysql_connect($connect,$user,$pass);
			mysql_select_db($base,$this->connexion);
			mysql_query("SET NAMES 'utf8'");
			// exécution de la requête

			$this->result_set=mysql_query($this->requete);



		}



		function rendre_html($url)
		{
			$m="<form method='post' action='".$_SERVER['SCRIPT_NAME']."'>";
			//$m.="<input type='hidden' name='sup_id'>";
			$m.="<div class='pres'>";

			// première ligne
			$m.="<ul>";


			for($i=0; $i<count($this->colonnes); $i++)
				$m.="<li>".$this->colonnes[$i]."</li>";

			$m.="</ul><br>";

			// données
			while( $ligne=mysql_fetch_array($this->result_set ))
			{
				$m.="<ul>";

				// colonnes de données
				do {
					$i=key($ligne);
					if(is_numeric($i))
						$m.="<li>$ligne[$i]</li>";
				} while(next($ligne));
				$m.="</ul><br>";
			}
			$m.="</div>";
			$m.="</form>";
			return $m;
		}


	}
?>
