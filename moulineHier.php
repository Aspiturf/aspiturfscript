<?php
/**
* This file is part of Aspiturf.
*
* Aspiturf is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Aspiturf is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Aspiturf.  If not, see <http://www.gnu.org/licenses/>.
**/

session_start();
$time = microtime(TRUE);
include "config.php";
include "bases.php";



$today=$argv[1];
// $today='2012-09-21';

echo $today." ";
echo " DEBUT COPIE ";
$copieDinfo=new MyTable("$bddpt","$connect","$user","$pass","SELECT comp, jour, heure, hippo, reun as reunion, prix as num, prixnom as prix, typec as discipline, partant, handi, reclam as reclamer, dist, groupe, corde,meteo, autos, cheque as alloc, courseabc, europ, natpis, amat, pistegp, arriv, lice FROM caractRap WHERE jour = '$today'");
while( $copie=mysql_fetch_array($copieDinfo->result_set ))
{
		if (is_numeric($copie['reunion']))  //verifie que reun est un nombre
		{
			$pmu="oui";
		} else {
			$pmu="non";
		}

		$sql = "UPDATE cachedate SET jour='$copie[jour]', pmu='$pmu', heure='$copie[heure]', hippo='$copie[hippo]', reunion='$copie[reunion]', num='$copie[num]', prix='$copie[prix]', discipline='$copie[discipline]', partant='$copie[partant]', handi='$copie[handi]', reclamer='$copie[reclamer]', dist='$copie[dist]', groupe='$copie[groupe]', corde='$copie[corde]',meteo='$copie[meteo]', autos='$copie[autos]', alloc='$copie[alloc]', categorie='$copie[courseabc]', europ='$copie[europ]', natpis='$copie[natpis]', amat='$copie[amat]', pistegp='$copie[pistegp]', arrive='$copie[arriv]', lice='$copie[lice]' WHERE numcourse = '$copie[comp]'";
		$resultat = mysql_db_query($bddpt, $sql);
		if (!$resultat)
			{
			    die('Requête invalide : ' . mysql_error());
			}
}

echo " COPIE FAITE ";
echo " Copie: ".round(microtime(TRUE) - $time ,3);
// Lit fichier recap1.txt qui contient les comp et les noms des chevaux
$Fichier = "/home/zil/aspiturf/paristurf1/recap1.txt";
if (is_file($Fichier)) {
	if ($TabFich = file($Fichier)) {
		for($i = 0; $i < count($TabFich); $i++) {
			//Attention, si course est arrivé, ent=4 et propriétaire=3, A REVOIR
			$ligne = $TabFich[$i];
			$tabligne = explode("@",$ligne);
			//echo $tabligne[1].$tabligne[0];
			$chev = $tabligne[1];
			$comp = $tabligne[0];
			$joc = $tabligne[2];
			$entrai = $tabligne[3];
			$prop = $tabligne[4];
		 	echo " Cheval= ".$chev." "." Comp= ".$comp." ";

			$recence=new MyTable("$bddpt","$connect","$user","$pass","SELECT cheval, jour, hippo, alloc, partant, dist, cl, cotedirect, jockey, prop, entraineur FROM cachedate USE index(chejou) WHERE cheval = '$chev' AND jour < '$today' ORDER BY jour DESC limit 0,1");
			$relarecence=mysql_fetch_array($recence->result_set );
			//différence entre 2 dates pour la récence cheval
			$nb_jours = round(((strtotime($today) - strtotime($relarecence['jour']))/(60*60*24)-1))+1;
			$dernierhippo = $relarecence['hippo'];
			$dernierealloc 	= $relarecence['alloc'];
			$derniernbpartants = $relarecence['partant'];
			$dernieredist = $relarecence['dist'];
			$derniereplace = $relarecence['cl'];
			$dernierecote = $relarecence['cotedirect'];
			$dernierProp = addslashes($relarecence['prop']);
			$dernierEnt = addslashes($relarecence['entraineur']);
			$dernierJoc = addslashes($relarecence['jockey']);



			//calcul musique
			$place = 0;
			$vict = 0;
			$ii=0;
			$mumu = "";
			$musii = array();
			//a revoir *-*-*-*-*-*-*-*-*-*-*-*-*
			$musii[0]="";$musii[1]="";$musii[2]="";$musii[3]="";$musii[4]="";$musii[5]="";
			$omfm = 0; $moy = 0;
			$anneejour = date("Y", strtotime($today));
			$anneejour = preg_replace('#20#','',$anneejour);

			$musique=new MyTable("$bddpt","$connect","$user","$pass","SELECT cheval, cl, discipline, jour FROM cachedate USE index(chejou) WHERE cheval = '$chev'  AND jour < '$today' ORDER BY jour DESC ");


			$nbCourse = mysql_num_rows($musique->result_set ); //compte le nbre de ligne = le nombre de course
			while( $rela=mysql_fetch_array($musique->result_set ))
			{
				if ($rela['cl'] == '1er') $vict++; //compte le nbre de victoire
				if (($rela['cl'] == '1er')|($rela['cl'] == '2ème')|($rela['cl'] == '3ème')) $place++; //compte le nbre de place

				$pl=preg_replace('#[^0-9]#','',$rela['cl']);
				$pl=preg_replace('` `','',$pl);
				$ty=preg_replace('#[^A-Z]#','',$rela['discipline']);
				$ty=strtolower($ty); //passe en minuscule
				$anneetype =  date("Y", strtotime($rela['jour']));
				$anneetype = preg_replace('#20#','',$anneetype);
				if (($pl==0)|($pl>10)) {
					$musii[$ii]=10;
				} else {
					$musii[$ii]=$pl;
				}
				$moy=$moy+$musii[$ii];
				if (($pl==1)|($pl==2)|($pl==3)) $omfm=$omfm+1;
				if ($pl==1) $ww=1;
				if ($pl=="") $pl=0;
				if ( $anneetype < $anneejour ) {
					$mumu.="($anneetype) $pl$ty ";
					$anneejour = $anneetype;
				} else {
					$mumu.="$pl$ty ";
				}

				$ii++;
			}
			echo " Musique: ".round(microtime(TRUE) - $time ,3);

			//calcul jockey
			$victJoc =0; $placeJoc =0;
			$victJocJour = 0; $placeJocJour = 0; $couruejockeyjour = 0;
			$ii=0;
			$mujoc = "";

			$musJockey=new MyTable("$bddpt","$connect","$user","$pass","SELECT jockey, cl, discipline, jour FROM cachedate USE index(jocjou) WHERE jockey = '$joc' AND jour <= '$today' ORDER BY jour DESC, heure DESC ");
			$nbCourseJoc = mysql_num_rows($musJockey->result_set ); //compte le nbre de ligne = le nombre de course
			while( $relaJoc=mysql_fetch_array($musJockey->result_set ))
			{
				if ($relaJoc['cl'] == '1er') $victJoc++; //compte le nbre de victoire
				if (($relaJoc['cl'] == '1er')|($relaJoc['cl'] == '2ème')|($relaJoc['cl'] == '3ème')) $placeJoc++; //compte le nbre de place

				if ($ii < 25) { //juste les 25 dernieres musiques
					$pl=preg_replace('#[^0-9]#','',$relaJoc['cl']);
					$pl=preg_replace('` `','',$pl);
					$ty=preg_replace('#[^A-Z]#','',$relaJoc['discipline']);
					$ty=strtolower($ty); //passe en minuscule
					if (($pl==0)|($pl>10)|($pl=='')) {
						$pl=10;
					}
					$mujoc.="$pl$ty ";
				}
				$ii++;
			}






			//Jockey, course du jour
			$musjourJoc=new MyTable("$bddpt","$connect","$user","$pass","SELECT jockey, cl, discipline, jour, hippo FROM cachedate USE index(jocjou) WHERE jockey = '$joc' and jour='$today'");
			$nbCourseJocJour = mysql_num_rows($musjourJoc->result_set ); //compte le nbre de ligne = le nombre de course
			while( $relaJoc=mysql_fetch_array($musjourJoc->result_set ))
			{
				$hippo= $relaJoc['hippo'];
				if ($relaJoc['cl'] == '1er') $victJocJour++; //compte le nbre de victoire du jour
				if ($relaJoc['cl'] != '') $couruejockeyjour++; //compte le nbre de course deja courue
			}

			echo " Jockey: ".$joc.round(microtime(TRUE) - $time ,3);


			//calcul % victoire sur l'hippo pour les JOCKEY
			$nbCourseJocHippo = 0; $victJocHippo = 0; $pourcVictHippo=0;
			$produitjoc=new MyTable("$bddpt","$connect","$user","$pass","select jockey, cl, hippo FROM cachedate where jockey='$joc' and hippo='$hippo'");
			$nbCourseJocHippo = mysql_num_rows($produitjoc->result_set ); //compte le nbre de ligne = le nombre de course
			while( $relajoc=mysql_fetch_array($produitjoc->result_set))
			{
				if ($relajoc['cl'] == '1er') $victJocHippo++; //compte le nbre de victoire sur cet hippo

				//Calcul des % victoire pour l'hippo
				if ($nbCourseJocHippo != 0 ) {
					$pourcVictHippo=round($victJocHippo/$nbCourseJocHippo*100,2);
				} else {
					$pourcVictHippo=0;
				}
			}




			//calcul Entraineur
			$victEntrai =0; $placeEntrai =0;
			$victEntraiJour = 0; $placeEntraiJour = 0; $courueEntraijour = 0;
			$ii=0;
			$muEntrai = "";

			$musEntrai=new MyTable("$bddpt","$connect","$user","$pass","SELECT entraineur, cl, discipline, jour FROM cachedate USE index(entjou) WHERE entraineur = '$entrai' AND jour < '$today' ORDER BY jour DESC, heure DESC ");
			$nbCourseEntrai = mysql_num_rows($musEntrai->result_set ); //compte le nbre de ligne = le nombre de course
			while( $relaEntrai=mysql_fetch_array($musEntrai->result_set ))
			{
				if ($relaEntrai['cl'] == '1er') $victEntrai++; //compte le nbre de victoire
				if (($relaEntrai['cl'] == '1er')|($relaEntrai['cl'] == '2ème')|($relaEntrai['cl'] == '3ème')) $placeEntrai++; //compte le nbre de place

				if ($ii < 25) { //juste les 25 dernieres musiques
					$pl=preg_replace('#[^0-9]#','',$relaEntrai['cl']);
					$pl=preg_replace('` `','',$pl);
					$ty=preg_replace('#[^A-Z]#','',$relaEntrai['discipline']);
					$ty=strtolower($ty); //passe en minuscule
					if (($pl==0)|($pl>10)|($pl=='')) {
						$pl=10;
					}
					$muEntrai.="$pl$ty ";
				}
				$ii++;
			}




			//Entraineur, course du jour
			$musjourEntrai=new MyTable("$bddpt","$connect","$user","$pass","SELECT entraineur, cl, discipline, jour, hippo FROM cachedate USE index(entjou) WHERE entraineur = '$entrai' and jour='$today'");
			$nbCourseEntraiJour = mysql_num_rows($musjourEntrai->result_set ); //compte le nbre de ligne = le nombre de course
			while( $relaEntrai=mysql_fetch_array($musjourEntrai->result_set ))
			{
				$hippo=$relaEntrai['hippo'];
				if ($relaEntrai['cl'] == '1er') $victEntraiJour++; //compte le nbre de victoire du jour
				if ($relaEntrai['cl'] != '') $courueEntraijour++; //compte le nbre de course deja courue
			}
			echo " Entrai: ".$entrai." ".round(microtime(TRUE) - $time ,3);







			//calcul % victoire sur l'hippo pour les ENTRAINEURS
			$nbCourseEntHippo = 0; $victEntHippo = 0; $pourcVictEntHippo=0;
			$produitent=new MyTable("$bddpt","$connect","$user","$pass","select entraineur, cl, hippo FROM cachedate WHERE entraineur='$entrai' and hippo='$hippo'");
			$nbCourseEntHippo = mysql_num_rows($produitent->result_set ); //compte le nbre de ligne = le nombre de course
			while( $relaent=mysql_fetch_array($produitent->result_set))
			{
				if ($relaent['cl'] == '1er') $victEntHippo++; //compte le nbre de victoire sur cet hippo

				//Calcul des % victoire pour l'hippo
				if ($nbCourseEntHippo != 0 ) {
					$pourcVictEntHippo=round($victEntHippo/$nbCourseEntHippo*100,2);
				} else {
					$pourcVictEntHippo=0;
				}
			}
			echo " calcul % victoire sur l'hippo: ".$entrai." ".round(microtime(TRUE) - $time ,3);


			//Calcul Propietaire
			$musjourProp=new MyTable("$bddpt","$connect","$user","$pass","SELECT prop FROM cachedate USE index(propjou) WHERE prop = '$prop' and jour='$today'");
			$nbCoursePropJour = mysql_num_rows($musjourProp->result_set ); //compte le nbre de ligne = le nombre de course
			//echo "prop:".$prop.$nbCoursePropJour." ";
			echo " Prop: ".$prop." ".round(microtime(TRUE) - $time ,3);

			$sqll = "UPDATE cachedate SET recence = '$nb_jours', coursescheval='$nbCourse', victoirescheval='$vict', placescheval='$place', musiqueche = '$mumu', m1 = '$musii[0]', m2 = '$musii[1]', m3 = '$musii[2]', m4 = '$musii[3]', m5 = '$musii[4]', m6 = '$musii[5]', coursesjockey='$nbCourseJoc', victoiresjockey='$victJoc', placejockey='$placeJoc', montesdujockeyjour='$nbCourseJocJour', couruejockeyjour='$couruejockeyjour', victoirejockeyjour='$victJocJour', musiquejoc='$mujoc', musiqueent='$muEntrai', monteentraineurjour='$nbCourseEntraiJour',	courueentraineurjour='$courueEntraijour', victoireentraineurjour='$victEntraiJour', coursesentraineur='$nbCourseEntrai', victoiresentraineur='$victEntrai', placeentraineur='$placeEntrai', nbcoursepropjour='$nbCoursePropJour', dernierhippo='$dernierhippo', dernierealloc='$dernierealloc',	derniernbpartants='$derniernbpartants',	dernieredist='$dernieredist', derniereplace='$derniereplace', 	dernierecote='$dernierecote', dernierJoc='$dernierJoc', dernierEnt='$dernierEnt', dernierProp='$dernierProp', pourcVictEntHippo='$pourcVictEntHippo', pourcVictHippo='$pourcVictHippo' WHERE comp='$comp'";
			$resultat = mysql_db_query($bddpt, $sqll);
			if (!$resultat)
			{
			    die('Requête invalide : ' . mysql_error());
			}


		} //fin for

	}
	else {
	echo "Le fichier ne peut être lu...<br>";

	}
}

else
{	echo "Désolé le fichier n'est pas valide<br>";
}

?>
